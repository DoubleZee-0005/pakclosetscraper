import requests
from bs4 import BeautifulSoup
import mysql.connector
from re import search

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database=" pakcloset",
)

url = "https://www.limelight.pk/"

r = requests.get(url)
htmlContent = r.content

# navLinksArray = []
subNavLinksArray = []

soup = BeautifulSoup(htmlContent, 'html.parser')

# navLinks = soup.select('li[class^="dropdown"] > a')

# for navLink in navLinks:
#     if str(navLink).find("https:") != -1:
#         navLinksArray.append(navLink['href'])
#     else:
#         navLinksArray.append("https://www.limelight.pk" + navLink['href'])

# for navLink in navLinksArray:
#     mycursor = mydb.cursor()
#     links = navLink
#     sql = "INSERT INTO `categories`   (`link`,`brand`) VALUES ('" + links + "','limelight')"
#     mycursor.execute(sql)

#     mydb.commit()

subNavLinks = soup.select('ul[class^="submenu"] > li > a')

for subNavLink in subNavLinks:
    link = str(subNavLink['href'])
    if ('https://www.limelight.pk' + link) not in subNavLinksArray:   
        subNavLinksArray.append('https://www.limelight.pk' + link)
    else:
        print('repeated Links: ' + link)

for subNavLink in subNavLinksArray:
    mycursor = mydb.cursor()
    links = subNavLink
    sql = "INSERT INTO `categories`   (`link`,`brand`) VALUES ('" + links + "','limelight')"    
    mycursor.execute(sql)
    mydb.commit()

# for navLink in navLinksArray:
#     print("main link: " + navLink)

# for subLink in subNavLinksArray:
#     print("sub link: " + subLink)