import requests
from bs4 import BeautifulSoup
import mysql.connector
from re import search

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="pakcloset",
)

mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM `categories` ORDER BY `id` ASC ")

myresult = mycursor.fetchall()

productsPages = []


def scrapeProductLinks(mainUrl, pageNumber=1):
    url = mainUrl+"?page=%s" % pageNumber
    print("Scrapping: " + url)
    r = requests.get(url)
    htmlContent = r.content

    soup = BeautifulSoup(htmlContent, 'html.parser')

    nextPage = soup.select('a[class^="load-more"]')
    if nextPage != []:
        pageNumber = nextPage[0]['data-count']
        productsPages.append(url)
        url = mainUrl+"?page=%s" % pageNumber
        scrapeProductLinks(mainUrl, pageNumber)
    else:
        lastPage = int(pageNumber)
        print(str(lastPage))
        url = mainUrl+"?page=%s" % str(lastPage)
        productsPages.append(url)
        print("No more pages found")


def scarpeProductLink(url):
    # url = "https://www.limelight.pk/collections/sale-scarves-dupatta?page=3"
    cat = url.partition("limelight.pk")[2]
    print(cat)
    split_string = cat.split("?", 1)
    print(split_string[0])
    cat = split_string[0]
    r = requests.get(url)
    htmlContent = r.content
    soup = BeautifulSoup(htmlContent, 'html.parser')
    productData = soup.select(
        'div[class^="product desktop-3 tablet-half"] > div[class^="ci alche-ci"] > a')
    for product in productData:
        productLink = "https://www.limelight.pk" + str(product['href'])
        print(productLink)
        sql = "INSERT INTO `links`(`urls`,`cat`,`brand`) VALUES ('" + \
            productLink + "','"+cat+"','limelight')"
        mycursor.execute(sql)
        mydb.commit()

        # r = requests.get(productLink)
        # htmlContent = r.content
        # soup = BeautifulSoup(htmlContent, 'html.parser')
        # title = soup.select(
        #     'div[id^="product-right"] > div[class^="section-title"] > h1')
        # print(title[0].text)


for x in myresult:
    scrapeProductLinks(x[1])

for productsPage in productsPages:
    scarpeProductLink(productsPage)

# # productLinks = soup.select('div[class^="ci alche-ci"] > a');

# # for productLink in productLinks:
# #     print("https://www.limelight.pk" + productLink['href']);
