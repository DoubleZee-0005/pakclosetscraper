from os import replace, stat
import requests
from bs4 import BeautifulSoup
import mysql.connector
from re import search
import csv
from datetime import datetime

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="pakcloset",
)

mycursor = mydb.cursor()

mycursor.execute(
    "SELECT * FROM `links` WHERE `brand` LIKE 'limelight' ORDER BY `id` ASC ;")

myresults = mycursor.fetchall()


for myresult in myresults:

    r = requests.get(
        'https://www.limelight.pk/collections/trousers-1/products/p4836tr-slw-bge')

    htmlContent = r.content
    soup = BeautifulSoup(htmlContent, 'html.parser')

    descriptionCsv = ""

    description = soup.select('div[class^="product-discription"]')
    if len(description):
        for descrip in description:
            for des in descrip.select("li"):
                des = des.text
                # des = des.replace('"', "")
                # des = des.replace("'", ".")
                descriptionCsv = descriptionCsv + " " + str(des)

    print(descriptionCsv)
    if "color: " in descriptionCsv:
        print(descriptionCsv.lower())
        print("Color is : ", descriptionCsv.lower().split(
            "color: ")[1].split()[0])
