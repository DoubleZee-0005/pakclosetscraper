from os import replace, stat
import requests
from bs4 import BeautifulSoup
import mysql.connector
from re import search
import csv
from datetime import datetime
import re

mydb = mysql.connector.connect(
    host="localhost",
    user="root",
    password="",
    database="pakcloset",
)

mycursor = mydb.cursor()

mycursor.execute(
    "SELECT * FROM `links` WHERE `brand` LIKE 'limelight' AND `id` > 3040 ORDER BY `id` ASC ;")

myresults = mycursor.fetchall()

file = open('products3.csv', 'w', newline='')
writer = csv.writer(file)
writer.writerow(['name', 'sku', 'price', 'url',
                 'cat', 'des', 'pic', 'slug', 'brand', 'images', 'size', 'design', 'color', 'pub', 'api', 'status', 'dt'])


def add_size_to_array(len_of_size, queryResult):
    if len_of_size >= 1:
        for sz in queryResult:
            sz = sz.text.strip()
            print(sz)
            sizes.append(sz)


for myresult in myresults:
    descriptionCsv = ""
    colorCsv = ""
    sizes = []

    print(myresult[0])
    sql = "UPDATE `links` SET `status`= 1 WHERE id = '"+str(myresult[0])+"'"
    mycursor.execute(sql)
    mydb.commit()
    print(myresult[1])
    print(myresult[2])
    split_string = myresult[2].split("?", 1)
    print(split_string[0])
    cat = split_string[0]
    r = requests.get(myresult[1])

    htmlContent = r.content
    soup = BeautifulSoup(htmlContent, 'html.parser')
    titleTag = soup.select(
        'div[id^="product-right"] > div[class^="section-title"] > h1')
    print(titleTag)
    if len(titleTag) > 0:
        title = titleTag[0].text
        slug = title.replace(" ", "-")
        print(titleTag[0].text)  # //====================title
    priceTag = soup.select(
        'span[class^="product-price"] > span[class^="money"]')
    if len(priceTag) > 0:
        price = priceTag[0].text.replace("Rs. ", "")
        print(price)  # //===================price
        # //===============image link
    images = soup.select(
        'div[id^="product-photos"] > div[id^="product-main"] > img')
    if images != None:
        dbImage = ""
        for image in images:
            img = ("https:/"+image['src'])
            dbImage = dbImage+img+"|"
        if images != []:
            pic = images[0]
            pic = pic['src']
        print(dbImage)

    skuTag = soup.select('span[class^="variant-sku"]')
    if len(skuTag) > 0:
        sku = skuTag[0].text  # //===============sku-variant
        sku = sku.replace(" ", "")
        print(sku)
    description = soup.select('div[class^="product-discription"]')
    if len(description):
        for descrip in description:
            for des in descrip.select("li"):
                des = des.text
                # des = des.replace('"', "")
                # des = des.replace("'", ".")
                descriptionCsv = descriptionCsv + " " + str(des)

            print(descriptionCsv)
            if "color: " in descriptionCsv.lower():
                colorCsv = descriptionCsv.lower().split(
                    "color: ")[1].split()[0]
                print(colorCsv)

    size = soup.select(
        'div[data-option-index^="0"] > div[class^="swatch-element"] > label')
    length_of_size = len(size)

    add_size_to_array(length_of_size, size)

    sizeForCsv = ""
    for size in sizes:
        sizeForCsv = size+","+sizeForCsv

    print(sizeForCsv)

    if titleTag != []:

        # header row
        pub = 0
        api = 0
        status = 0
        size = 0
        design = 0
        pub = 0
        api = 0
        status = 0
        now = datetime.now()
        dt = now.strftime("%H:%M:%S")
        print(descriptionCsv)
        writer.writerow(
            [title, sku, price,
             myresult[1], cat, descriptionCsv,
             pic, slug, 'limelight', dbImage, sizeForCsv, design, colorCsv, pub, api, status, dt])

        # mycursor = mydb.cursor()
        # sql = "INSERT INTO `data`(`name`,`sku`,`price`,`url`,`cat`,`des`,`pic`,`slug`,`brand`,`images`) VALUES ('"+title + \
        #     "','"+sku+"','"+price+"','" + \
        #     myresult[1]+"','"+cat+"','"+descriptionCsv + \
        #     "','"+pic+"','"+slug+"','limelight','"+dbImage+"')"
        # mycursor.execute(sql)
        # mydb.commit()

file.close()
